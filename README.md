# Server-side CRM authentication using Azure Active Directory

1. Load the project and make sure it compiles. VS2013 is recommended but it seems to work in VS2012 as well.

2. Register your application in Azure Active Directory
    1. In Azure go to Active Directory -> Applications -> Add.
    2. When prompted choose “Add an application my organization is developing”
    3. Next: enter name of your choice, select “Web application and/or web API” as type
    4. Next: APP URL is https://localhost:44300, APP ID URI is https://caseowary (latter must be unique within your org directory, use whatever you want and, as you can see, it’s URI not URL)
    5. Next: SINGLE SIGN-ON

3. Now you should see
    1. APP ID URI: https://caseowary. **IMPORTANT!** This is URI not URL so things like trailing slash matter. Don’t waste 2 extra hours as I did.
    2. FEDERATION METADATA URL: https://login.windows.net/<your_tenant_guid>/FederationMetadata/2007-06/FederationMetadata.xml. 

4.	Click CONFIGURE
    1. Scroll to CLIENT ID (1) and copy it to web.config
    2. Select duration under KEYS (2)
    3. Change REPLY URL (3) to https://localhost:44300/reply. That goes into web.config
    4. Hit Save. Copy key value from KEYS section into web.config. **IMPORTANT!** Message does not lie about this is the last time you see this key so copy it now.

![screenshot](https://bitbucket.org/Georged/caseowary/raw/master/readme.png)

## web.config entries

    <appSettings>
        <add key="ida:Realm" value="3.1_app_id_uri" />
        <add key="ida:AudienceUri" value="3.1_app_id_uri" />
        <add key="TenantId" value="3.2_your_tenant_guid" />
        <add key="ClientId" value="4.1_client_id" />
        <add key="RedirectUrl" value="4.3_reply_url" />
        <add key="AuthorizationKey" value="4.4_authorization_key" />
        <!-- Self-explanatory entries --> 
		<add key="OrgUrl" value="https://yourorg.crm.dynamics.com/" />
		<add key="ODataUrl" value="https://yourorg.crm.dynamics.com/XRMServices/2011/OrganizationData.svc/" />
		<add key="OrgSvcUrl" value="https://yourorg.api.crm.dynamics.com/XRMServices/2011/Organization.svc/web" />
