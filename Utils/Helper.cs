﻿using Caseowary.Utils;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Xml.Linq;

namespace Caseowary.Xrm
{
    public static class Helper
    {
        static CRMOData.CrmOrgContext _oDataService = null;
        public static CRMOData.CrmOrgContext GetODataService(string AccessToken)
        {
            if (_oDataService == null)
            {
                _oDataService = new CRMOData.CrmOrgContext(new Uri(ConfigurationManager.AppSettings["ODataUrl"]));
                _oDataService.SendingRequest2 += (sender, args) => args.RequestMessage.SetHeader("Authorization", "Bearer " + AccessToken);
            }

            return _oDataService;
        }

        public static string GetUserName(string token, Guid userId)
        {
            var service = Helper.GetODataService(token);
            var username = service.SystemUserSet
                .Where(u => u.SystemUserId == userId)
                .Select(u => u.FullName)
                .ToList()
                .FirstOrDefault();
            return username;
        }

        public static Guid GetUserId(string token)
        {
            var WhoAmIResponseBody = HttpRequestBuilder.WhoAmIRequest(token);
            XDocument xdoc = XDocument.Parse(WhoAmIResponseBody, LoadOptions.None);

            string ns = @"http://schemas.datacontract.org/2004/07/System.Collections.Generic";
            XName keyXName = XName.Get("key", ns);
            XName valueXName = XName.Get("value", ns);
            // I'm a lousy XPerson - no idea how to do this better
            string id = xdoc.Descendants(keyXName)
                            .Where( k => k.Value == "UserId")
                            .First()
                            .Parent
                            .Element(valueXName).Value;
            return Guid.Parse(id);
        }
    }
}