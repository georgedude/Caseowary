﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IdentityModel.Claims;
using System.IdentityModel.Services;
using System.Linq;
using System.Web.Helpers;
using System.Web;
using Caseowary.Utils;
using Microsoft.IdentityModel.Clients.ActiveDirectory;

namespace Caseowary
{
    public class IdentityConfig
    {
        public static string AudienceUri { get; private set; }
        public static string Realm { get; private set; }
        public static string Tenant { get; private set; }

        public AuthenticationResult TokenResult { get; private set; }

        public static IdentityConfig Current
        {
            get
            {
                if (HttpContext.Current != null)
                {
                    IdentityConfig ic = HttpContext.Current.Session["IdentityConfig"] as IdentityConfig;
                    if (ic == null)
                    {
                        ic = new IdentityConfig();
                        HttpContext.Current.Session.Add("IdentityConfig", ic);
                    }
                    return ic;
                }
                else
                    return null;
            }
        }

        public static void ConfigureIdentity()
        {
            Tenant = ConfigurationManager.AppSettings["TenantId"];

            // Set the realm for the application
            Realm = ConfigurationManager.AppSettings["ida:realm"];

            // Set the audienceUri for the application
            AudienceUri = ConfigurationManager.AppSettings["ida:AudienceUri"];
            if (!String.IsNullOrEmpty(AudienceUri))
            {
                UpdateAudienceUri();
            }

            AntiForgeryConfig.UniqueClaimTypeIdentifier = ClaimTypes.Name;
            FederatedAuthentication.FederationConfiguration.WsFederationConfiguration.Issuer = string.Format("https://login.windows.net/{0}/wsfed", Tenant);
            FederatedAuthentication.FederationConfiguration.WsFederationConfiguration.Realm = Realm;
        
            // Alternative implementation is to use ValidatingIssuerNameRegistry.WriteToConfig(metadataAddress, configPath)
            // when application starts, see global.asx.cs
            // 
            // AddFederationThumbs();
        }

        private static void AddFederationThumbs()
        {
            // iideally, thumbprints should be in config file but we'd like to do this at runtime
            // so we get all thumbprints for the issuing authority and add our autority as a trusted issuer
            string federationMetadataLocation = string.Format("https://login.windows.net/{0}/FederationMetadata/2007-06/FederationMetadata.xml", Tenant);
            var ia = System.IdentityModel.Tokens.ValidatingIssuerNameRegistry.GetIssuingAuthority(federationMetadataLocation);
            var ir = FederatedAuthentication.FederationConfiguration.IdentityConfiguration.IssuerNameRegistry as System.IdentityModel.Tokens.ConfigurationBasedIssuerNameRegistry;

            if (ia != null && ir != null)
            {
                foreach (var thumb in ia.Thumbprints)
                {
                    ir.AddTrustedIssuer(thumb, ia.Name);
                }
            }
        }

        public static void UpdateAudienceUri()
        {
            int count = FederatedAuthentication.FederationConfiguration.IdentityConfiguration
                .AudienceRestriction.AllowedAudienceUris.Count(
                    uri => String.Equals(uri.OriginalString, AudienceUri, StringComparison.OrdinalIgnoreCase));
            if (count == 0)
            {
                FederatedAuthentication.FederationConfiguration.IdentityConfiguration
                    .AudienceRestriction.AllowedAudienceUris.Add(new Uri(AudienceUri));
            }
        }

        public void AcquireToken(string code)
        {
            string tenantId = System.Security.Claims.ClaimsPrincipal.Current.FindFirst(SAMLClaimTypes.TenantId).Value;
            string clientId = ConfigurationManager.AppSettings["ClientId"];
            string AppKey = ConfigurationManager.AppSettings["AuthorizationKey"];
            var redirectUrl = ConfigurationManager.AppSettings["RedirectUrl"];

            AuthenticationContext ac = new AuthenticationContext(string.Format("https://login.windows.net/{0}", tenantId));
            ClientCredential creds = new ClientCredential(clientId, AppKey);

            TokenResult = ac.AcquireTokenByAuthorizationCode(code, new Uri(redirectUrl), creds);
            // var refreshedResult = ac.AcquireTokenByRefreshToken(TokenResult.RefreshToken, clientId, creds);
        }

        public bool IsAuthenticated
        {
            get
            {
                return TokenResult != null;
            }
        }

        public string GetAuthorizationUrl()
        {
            string clientId = ConfigurationManager.AppSettings["ClientId"];
            string tenantId = System.Security.Claims.ClaimsPrincipal.Current.FindFirst(SAMLClaimTypes.TenantId).Value;
            string AppKey = ConfigurationManager.AppSettings["AuthorizationKey"];
            string redirectUrl = ConfigurationManager.AppSettings["RedirectUrl"];

            return string.Format(
                            "https://login.windows.net/{0}/oauth2/authorize?api-version=1.0&response_type=code&client_id={1}&resource={2}&redirect_uri={3}",
                        tenantId,
                        clientId,
                        "Microsoft.CRM",
                        HttpUtility.UrlEncode(redirectUrl)
                        );
        }

        internal void ResetTokens()
        {
            TokenResult = null;
        }

        public static string ReturnUrl { get; set; }
    }

}
