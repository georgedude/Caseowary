﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Caseowary.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Caseowary is confined to the jungle areas of North Queensland.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Where to find caseowaries.";

            return View();
        }
        public ActionResult SignIn()
        {
            // simply redirect to root (after the login). login will be forced by <authorization> element in web.config
            return new RedirectResult("/");
        }

    }
}